const chai = require("chai");
const chaihttp = require("chai-http");
const { should, expect } = chai;

chai.use(chaihttp);
const server = require("../index");

// const User = require("../models/user");
const bcrypt = require('bcryptjs');
let salt = bcrypt.genSaltSync(10);

const fixtures = require("../fixtures/userFixtures");
let user = fixtures.create();
let wrongUser = fixtures.create();

user={
  name:user.name,
  email:user.email,
  password:bcrypt.hashSync(user.password,salt)
}

wrongUser={
  name:wrongUser.name,
  email:wrongUser.email,
  password:bcrypt.hashSync(wrongUser.password,salt)
}

const wrongPassword = {
  name:user.name,
  email:user.email,
  password:wrongUser.password
}
let _idUser ="";
let randomId = "123";

describe("Test User", () => {
  // before(function() {
  //   User.deleteMany({}).then(() => done());
  // });
  // after(function() {
  //   User.deleteMany({}).then(() => done());
  // });

  context("Register user", () => {
    it("Should create new user", function() {
      chai
        .request(server)
        .post("/api/v1/users")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(user))
        .end(function(err, res) {
          let { status, data } = res.body;     
          expect(res.status).to.eq(201);
          expect(status).to.eq(true);
          expect(data).to.be.an("object");
          expect(data).to.have.property("_id");
          expect(data).to.have.property("email");
          expect(data.email).to.eq(user.email);        
        });
    });

    it("Shouldn't create new user", function() {
      chai
        .request(server)
        .post("/api/v1/users")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(user))
        .end(function(err, res) {
          let { status, errors } = res.body;
          expect(res.status).to.eq(422);
          expect(status).to.eq(false);
          expect(errors).to.be.an("object");       
        });
    });
  });

  context("Login user", () => {
    it("Should success to login", function() {
      chai
        .request(server)
        .post("/api/v1/users-login")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(user))
        .end(function(err, res) {
            let { status, data } = res.body;
            expect(res.status).to.eq(200); 
            expect(status).to.eq(true);
            expect(data).to.be.a('string');   
        });
    });

    it('Shouldn\'t found email',function(){
      chai
        .request(server)
        .post('/api/v1/users-login')
        .set("Content-Type","application/json")
        .send(JSON.stringify(wrongUser))
        .end(function(err,res){
          let {status,errors}=res.body;
          expect(res.status).to.eq(404);
          expect(status).to.eq(false);
          expect(errors).to.be.a('string');
      });
    });

    it('Shouldn\'t match password', function(){
      chai
        .request(server)
        .post('/api/v1/users-login')
        .set("Content-Type","application/json")
        .send(JSON.stringify(wrongPassword))
        .end(function(err,res){
          let{status,errors}=res.body;
          expect(res.status).to.eq(406);
          expect(status).to.eq(false);
          expect(errors).to.be.a('string');          
      })
    })
  });

  context("Get all user",()=>{
    it('Should get all user',function(){
      chai
        .request(server)
        .get('/api/v1/users')
        .set("Content-Type","application/json")
        .end(function(err,res){
          let {status,data}=res.body;
          expect(res.status).to.eq(200);
          expect(status).to.eq(true);
          expect(data).to.be.an('array');
          data = data.map(item=>{
            return {
              _id:item._id,
              name:item.name,
              email:item.email
            };
          });
          data.forEach(element => {
            expect(element).to.have.property("_id");
            expect(element).to.have.property("name");
            expect(element).to.have.property("email");
          });          
        })
    })
  });
  
  // context("Update user by id",()=>{
  //   it('Should update user by id',function(){
  //     console.log("0");
  //     chai
  //       .request(server)
  //       .put(`/api/v1/users/${_idUser}`)
  //       .set("Content-Type","application/json")
  //       .send(JSON.stringify(wrongUser))
  //       .end(function(err,res){
  //         let {status,data}=res.body;
  //         expect(res.status).to.eq(200);
  //         expect(status).to.eq(true);
  //         expect(data).to.be.a('string');          
  //       })
  //   })

  //   it('Shouldn\'t update user by id',function(){
  //     chai
  //       .request(server)
  //       .put(`/api/v1/users/${randomId}`)
  //       .set("Content-Type","application/json")
  //       .send(JSON.stringify({}))
  //       .end(function(err,res){
  //         let {status,errors} = res.body;
  //         expect(res.status).to.eq(404);
  //         expect(status).to.eq(false);
  //         expect(errors).to.be.an('object');          
  //       })
  //   })
  // })

  // context("Delete user by id",()=>{
  //   it('Should Delete user by id',function(){
  //     chai
  //       .request(server)
  //       .delete(`/api/v1/users/${_idUser}`)
  //       .end(function(err,res){
  //         let {status,data}=res.body;
  //         expect(res.status).to.eq(200);
  //         expect(status).to.eq(true);
  //         expect(data).to.be.a('string');
          
  //       })
  //   })

  //   it('Shouldn\'t delete user by id',function(){
  //     chai
  //       .request(server)
  //       .delete(`/api/v1/users/${randomId}`)
  //       .end(function(err,res){
  //         let {status,errors} = res.body;
  //         expect(res.status).to.eq(404);
  //         expect(status).to.eq(false);
  //         expect(errors).to.be.an('object');
          
  //       })
  //   })
  // })
});
