const Post= require('../models/post');
const User= require('../models/user');
const {success, error}=require('../helpers/response');

async function like (req,res){
    try{
        let post = await Post.findById(req.query._id);
        let user = await User.findById(req.headers.authorization);
        if(!post||!user){
            return error(res,'User or Post doesn\'t seem to be exist',422);
        }

        post.likes.push(user._id);
        await post.save();
        return success(res,post,201);
    }
    catch(err){
        return error(res,err,422);
    }
}

function createPost (req,res){
    if(!req.headers.authorization){
        return error(res,'Unauthorized',401);
    }

    let post = new Post({
        title : req.body.title,
        body : req.body.body,
        author : req.headers.authorization
    })
    post.save()
    .then(()=>{
        return success(res,post,201);
    })
    .catch(err=>{
        return error(res,err,422);
    })
}

function readPost (req,res){
    Post.all()
    .then(i=>{
        return success(res,i,200);
    })
    .catch(err=>{
        return error(res,err,404);
    })
}

function updatePost (req,res){
    Post.updateOne({_id:req.params._id},req.body)
    .then(i=>{
        return success(res,i,200);
    })
    .catch(err=>{
        return error(res,err,404);
    })
}

function deletePost(req,res){
    Post.deleteOne({_id:req.params._id})
    .then(i=>{
        return success(res,i,200);
    })
    .catch(err=>{
        return error(res,err,404);
    })
}

module.exports={
    createPost:createPost,
    readPost:readPost,
    updatePost:updatePost,
    deletePost:deletePost,
    likesPost:like
}