const express = require('express');
const router = express.Router();

const {
    createUser,
    readUser,
    updateUser,
    deleteUser,
    loginUser
} =require('../controller/userController.js');

const {
    createPost,
    readPost,
    updatePost,
    deletePost,
    likesPost
} =require('../controller/postController.js');

const authenticate=require('../middlewares/authentication.js');

//User router
router.post('/users',createUser);
router.get('/users',readUser);
router.put('/users/:_id',updateUser);
router.delete('/users/:_id',deleteUser);
router.post('/users-login',loginUser);

//Post router
router.post('/posts',authenticate,createPost);
router.get('/posts',authenticate,readPost);
router.put('/posts/:_id',authenticate,updatePost);
router.delete('/posts/:_id',authenticate,deletePost);
router.post('/posts-like',authenticate,likesPost);

module.exports=router;